import './App.css';
import {HomePage} from "./comnponents/HomePage";
import {SignUp} from "./comnponents/SignUp";
import {Login} from "./comnponents/Login";
import {Route, Routes} from "react-router-dom";

function App() {
  return (
      <div className="App" style={{width: "100%"}}>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/register" element={<SignUp/>} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </div>
  );
}

export default App;
