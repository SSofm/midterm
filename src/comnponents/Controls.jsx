import "../style/help.css"
export const Controls = () => {
    return (
        <div className="controls-block">
            <div className="controls ">
                <h3 className="title-controls">ORBIT NAVIGATION</h3>
                <b>Move camera:</b> 1-finger drag or Left Mouse Button <br></br>
                <b>Pan: </b> 2-finger drag or Right Mouse Button or <kbd>SHIFT</kbd> + Left Mouse
                Button
            </div>
            <div className="controls">
                <h3 className="title-controls">RENDERING</h3>
                <div>
                    <div className="controls-items">
                        <b>Wireframe:</b> 5
                    </div>
                    <div className="controls-items">
                        <b>Inspector:</b>1
                    </div>
                </div>
            </div>
            <div className="controls">
                <h3 className="title-controls">INSPECTOR</h3>
                <div>
                    <div className="controls-items">
                        <b>Render:</b> 1
                    </div>
                    <div className="controls-items">
                        <b>Materials:</b> 2
                    </div>
                    <div className="controls-items">
                        <b>Geometry:</b> 3
                    </div>
                    <div className="controls-items">
                        <b>UV:</b> 4
                    </div>
                    <div className="controls-items">
                        <b>Animation:</b> 5
                    </div>
                </div>
            </div>
            <div className="controls">
                <h3 className="title-controls">GENERAL</h3>
                <div className="controls-items">
                    <b>Fullscreen:</b> f
                </div>
            </div>
            <div className="controls">
                <h3 className="title-controls">SOUND</h3>
                <div className="controls-items">
                    <b>Mute:</b> <b>m</b>{" "}
                </div>
            </div>
        </div>
    );
};
