import {Navbar} from "./Navbar";
import {Footer} from "./Footer";
import React from "react";
import {Card} from "./Card";
import { Carousel } from "./Carousel";
import { Pagination } from "./Pagination";

export const HomePage = () => {
    return (
      <div>
        <Navbar />
        <Carousel />
        <div className="max-w-[1709px] ml-auto mr-auto">
          <div className="grid grid-cols-4 gap-5">
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
          </div>
        </div>
        <Pagination/>
        <Footer />
      </div>
    );
}