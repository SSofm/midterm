import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import { ModelPreview } from "./ModelPreview";
import { Avatar } from "./Avatar";
import { Comment } from "./Comment";
import { HorizontalCard } from "./HorizontalCard";
import '../style/responsive.css'
import image1 from "../assets/main-gallery/1.jpg";
import image2 from "../assets/main-gallery/2.png";
import image3 from "../assets/main-gallery/3.png";
import image4 from "../assets/main-gallery/4.jpg";
import {ModelInfor} from "./ModelInfor";

const imageList = [image1, image2, image3, image4];

const style = {
  position: "absolute",
  display: "flex",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "95%",
  height: "90%",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  overflowY: "auto",
};

export const Card = () => {
  const randomIndex = Math.floor(Math.random() * imageList.length);
  const randomImage = imageList[randomIndex];
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Box>
              <ModelPreview />
              <div className="flex justify-between">
                <div>
                  <div className="text-2xl font-medium dark:text-white mt-6">
                    Bàn ghế nhà ăn
                  </div>
                  <div>
                    <Avatar />
                  </div>
                </div>

              </div>
              <ModelInfor/>
              <Comment/>
            </Box>
            <Box sx={{ display: "flex", flexDirection: "column", gap: "20px" }}>
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
              <HorizontalCard />
            </Box>
          </Box>
        </Fade>
      </Modal>

      <div className="max-w-lg bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <img
          className="rounded-t-lg transition-all duration-300 cursor-pointer filter grayscale-0 hover:grayscale"
          src={randomImage}
          alt=""
          onClick={handleOpen}
        />

        <div className="p-5">
          <a href="/">
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
              Noteworthy technology acquisitions 2021
            </h5>
          </a>
          <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
            Here are the biggest enterprise technology acquisitions of 2021 so
            far, in reverse chronological order.
          </p>
          <a
            href="/"
            className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Read more
            <svg
              aria-hidden="true"
              className="w-4 h-4 ml-2 -mr-1"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </a>
        </div>
      </div>
    </>
  );
};
