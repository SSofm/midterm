import React from "react";

export const ModelInfor = () => {
  return (
    <div>
      <div
        style={{
          width: "100%"
        }}
      >
        <div>
          <p
            style={{
              marginTop: 30,
              fontSize: 20,
              fontWeight: 500,
              textAlign: "start",
            }}
          >
            Mô tả sản phẩm
          </p>
          <div
            style={{
              marginTop: 10,
            }}
          >
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Tên sản phẩm</p>
              <p> Bàn ghế nhà ăn</p>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Giá bán</p>
              <p> 200 USD</p>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Năm sản xuất</p>
              <p> 2021</p>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Người bán</p>
              <p>Nguyen Van Sang</p>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Tên sản phẩm</p>
              <p> Bàn ghế nhà ăn</p>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: 18,
                paddingBottom: 8,
                paddingRight: 20,
                paddingLeft: 20,
                marginBottom: 10,
                justifyContent: "space-between",
                borderBottom: "1px solid gray",
              }}
            >
              <p>Chất liệu</p>
              <p>Gỗ hương</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
