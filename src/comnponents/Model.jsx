import React from "react";
import { Html, useGLTF } from "@react-three/drei";
import * as THREE from "three";
import { Spinner } from "flowbite-react";

const Model = ({ url, rotation, wireframe, color }) => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [audio] = React.useState(new Audio("/sounds/sound.mp3"));
  const [scale, setScale] = React.useState(1);
  const gltf = useGLTF(url, true);

  const handleWheelPreventDefault = (event) => {
    // Ngăn chặn sự kiện wheel khi giá trị scale đã đạt đến giới hạn
    if (scale >= 0.5) {
      event.preventDefault();
    }
  };

  if (wireframe) {
    gltf.scene.traverse((node) => {
      if (!node.isMesh) return;

      node.material = new THREE.MeshBasicMaterial({
        color,
        wireframe,
      });
    });
  }

  React.useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
      audio.volume = 0.25;
      audio.play();
    }, 5000); // Đợi 5 giây trước khi hiển thị model

    return () => {
      clearTimeout(timer);
      audio.pause();
    };
  }, []);

  const handleWheel = (event) => {
    const newScale = event.deltaY > 0 ? scale * 0.9 : scale * 1.1; // Tính toán giá trị scale mới
    setScale(newScale);
    audio.volume = 0.25 * newScale; // Điều chỉnh âm lượng theo tỷ lệ của giá trị scale mới
  };
  return (
    <>
      <React.Fragment
        style={{ backgroundColor: "#faeee7", height: "100vh" }}
        shadows
        dpr={[1, 2]}
        onWheel={handleWheelPreventDefault}
      >
        <color attach="background" args={["#faeee7"]} />
        <mesh rotation={rotation} scale={[scale, scale, scale]} castShadow>
          {isLoading && (
            <Html>
              <div className="overlay">
                <Spinner />
              </div>
            </Html>
          )}

          {!isLoading && (
            <primitive
              object={gltf.scene}
              rotation={rotation}
              scale={[scale, scale, scale]}
              onWheel={handleWheel}
            />
          )}
          <meshStandardMaterial
            attach="material"
            color="gray"
            roughness={0.5}
            metalness={0.5}
            shadowSide={THREE.DoubleSide}
          />
        </mesh>
        <mesh
          rotation={[-Math.PI / 2, 0, 0]}
          position={[0, -0.1, 0]}
          receiveShadow
        >
          <planeBufferGeometry attach="geometry" args={[1000, 1000]} />
          <shadowMaterial attach="material" transparent opacity={0.3} />
        </mesh>
      </React.Fragment>
    </>
  );
};

export default Model;
